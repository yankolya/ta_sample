package com.examples.targetapp.service;

import com.examples.targetapp.dao.PatientDao;
import com.examples.targetapp.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService {

    @Autowired
    public PatientDao patientDao;

    public Patient getPatient(String id){
        return patientDao.getPatientById(id);
    }

    public List<Patient> getAllPatients(){
        return patientDao.getAllPatients();
    }
}
