package com.examples.targetapp.controller;

import com.examples.targetapp.model.Patient;
import com.examples.targetapp.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class PatientController {

    @Autowired
    public PatientService patientService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Patient getPatient(@PathVariable String id) {
		return patientService.getPatient(id);
	}

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody List<Patient> getAllPatients() {
        return patientService.getAllPatients();
    }

}