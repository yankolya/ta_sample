package com.examples.targetapp.dao;

import com.examples.targetapp.model.Patient;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class PatientDao {

    private Map<String, Patient> patientsMap = new HashMap<String, Patient>();
    private Patient[] patients = new Patient[]{
            new Patient("1","2433526","Jack Daniel's","New Mexico","Adrenal disorders","7/28/2014"),
            new Patient("2","4626632","Tom Johns","Oklahoma","Glucose homeostasis disorders","9/8/2014"),
            new Patient("3","2329234","Chuck Norris","Minnesota","Thyroid disorders","9/2/2014"),
            new Patient("4","8987743","Will Smith","New York","Pituitary gland disorders","9/20/2014"),
            new Patient("5","8238923","Bill Gates","New Mexico","Posterior pituitary","8/14/2014"),
            new Patient("6","2378402","John Smith","Kentucky","Sex hormone disorders","9/23/2014")
    };

    public PatientDao() {
        initializePatientsStorage();
    }

    private void initializePatientsStorage(){
        for (Patient patient : patients){
            patientsMap.put(patient.getId(), patient);
        }
    }

    public Patient getPatientById(String id){
        return patientsMap.get(id);
    }

    public List<Patient> getAllPatients(){
        return Arrays.asList(patients);
    }


}
