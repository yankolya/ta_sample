package com.examples.targetapp.model;

public class Patient {

    private String id;
    private String pcn;
    private String name;
    private String facility;
    private String illness;
    private String dischargeDate;

    public Patient(String id, String pcn, String name, String facility, String illness, String dischargeDate) {
        this.id = id;
        this.pcn = pcn;
        this.name = name;
        this.facility = facility;
        this.illness = illness;
        this.dischargeDate = dischargeDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPcn() {
        return pcn;
    }

    public void setPcn(String pcn) {
        this.pcn = pcn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getIllness() {
        return illness;
    }

    public void setIllness(String illness) {
        this.illness = illness;
    }

    public String getDischargeDate() {
        return dischargeDate;
    }

    public void setDischargeDate(String dischargeDate) {
        this.dischargeDate = dischargeDate;
    }
}
