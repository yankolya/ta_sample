<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Patient info</title>
</head>
<body>
<table>
    <thead></thead>
    <tbody>
    <tr>
        <td>PCN</td>
        <td>${patient.pcn}</td>
    </tr>
    <tr>
        <td>Patient Name</td>
        <td>${patient.name}</td>
    </tr>
    <tr>
        <td>Facility</td>
        <td>${patient.facility}</td>
    </tr>
    <tr>
        <td>Illness</td>
        <td>${patient.illness}</td>
    </tr>
    <tr>
        <td>Discharge date</td>
        <td>${patient.dischargeDate}</td>
    </tr>
    </tbody>
</table>
<a href="<c:url value="/"/>"> Home</a>
</body>
</html>
