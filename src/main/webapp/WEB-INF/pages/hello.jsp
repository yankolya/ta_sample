<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Patients worklist</title>
</head>
<body>
<table>
    <thead>
    <tr>
        <th>Patient Name</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${patients}" var="patient">
        <tr>
            <td><p>${patient.name}</p></td>
            <td><a href="<c:url value="${patient.id}" />"> Show patient info</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>